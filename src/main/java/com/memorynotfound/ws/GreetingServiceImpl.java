package com.memorynotfound.ws;

import javax.jws.WebService;

@WebService
public class GreetingServiceImpl implements GreetingService {

    public String printMessage() {
        return "Hello, World!";
    }
}
